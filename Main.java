package com.company.w7hw;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        System.out.println("Hello!\n" + "1)Show Senior Programmers\n" + "2)Show Junior Programmers\n" +"3)Show future seniors\n"+"4)Show total cost of company\n"+ "5)Exit");
        Scanner scan = new Scanner(System.in);
        Company company = new Company();                //create instances
        Company senior = new Senior();
        Company junior = new Junior();
        Senior future_senior = new Junior();
        Company cost = new Cost();
        while(true){                                     //work with console
            int number = scan.nextInt();
            if(number==1) {
                senior.EmployeeName();
            }
            else if (number==2) {
                junior.EmployeeName();
            }
            else if (number==3) {
                future_senior.FutureSeniors();
            }
            else if (number==4) {
                cost.Total_cost();
            }
            else if (number==5){
                System.exit(1);
            }
            else {
                System.out.println("Invalid number");
            }
        }
    }
}
